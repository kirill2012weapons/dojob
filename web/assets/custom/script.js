
$( document ).ready(function() {

    $('[data-dismiss="fileinput"]').on('click' ,function (event) {
        $('[data-blah]')
            .attr('src', '/assets/custom/img/nobody_m.original.jpg');
        $('[data-file-log]').val('');
    });


    window.readURL = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('[data-blah]')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
});