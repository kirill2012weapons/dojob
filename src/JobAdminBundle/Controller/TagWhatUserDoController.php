<?php

namespace JobAdminBundle\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use JobAdminBundle\Entity\Tag;
use JobAdminBundle\Form\FormType\TagType;
use JobAdminBundle\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TagWhatUserDoController extends Controller
{
    public function showTags(Request $request)
    {
        /**
         * @var $em                     EntityManager
         * @var $tagsRep                TagRepository
         */

        $em = $this->getDoctrine()->getManager();

        $tag = new Tag();
        $tag->setSlug(Tag::TAG_WHAT_USER_DO);
        $formAddTag = $this->createForm(TagType::class, $tag);

        $formAddTag->handleRequest($request);

        if ($formAddTag->isSubmitted() && $formAddTag->isValid()) {

            $em->persist($tag);
            try {
                $em->flush();
                $this->get('alert')->success('The tag "' . $tag->getTagName() . '" has been added.');
                return $this->redirectToRoute('job_admin_tags_what_user_do_show');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error($exception->getMessage(), 'Tag not added.');
                return $this->redirectToRoute('job_admin_tags_what_user_do_show');
            }

        }

        $tagsRep = $em->getRepository(Tag::class);
        $tagList = $tagsRep->loadAllTagsAsArray(Tag::TAG_WHAT_USER_DO);

        return $this->render('@view.job_admin/TagWhatUserDo/showTags.html.twig', [
            'formAddTag'            => $formAddTag->createView(),
            'tagList'               => $tagList,
        ]);
    }

    public function removeTags(Request $request, $id)
    {
        /**
         * @var $em                     EntityManager
         * @var $tag                    Tag
         */
        $em = $this->getDoctrine()->getManager();

        try {
            $tag = $em->find(Tag::class, $id);
        } catch (\Exception $exception) {
            $this->get('alert')->error($exception->getMessage());
            return $this->redirectToRoute('job_admin_tags_what_user_do_show');
        }

        $em->remove($tag);
        try {
            $em->flush();
            $this->get('alert')->success('The tag "' . $tag->getTagName() . '" deleted!');
            return $this->redirectToRoute('job_admin_tags_what_user_do_show');
        } catch (\Doctrine\ORM\OptimisticLockException $exception) {
            $this->get('alert')->error($exception->getMessage());
            return $this->redirectToRoute('job_admin_tags_what_user_do_show');
        }

    }

    public function modifyTags(Request $request, $id)
    {
        /**
         * @var $em                     EntityManager
         * @var $tagsRep                TagRepository
         */

        $em = $this->getDoctrine()->getManager();

        try {
            $tag = $em->find(Tag::class, $id);
        } catch (\Exception $exception) {
            $this->get('alert')->error($exception->getMessage());
            return $this->redirectToRoute('job_admin_tags_what_user_do_show');
        }
        $tag->setSlug(Tag::TAG_WHAT_USER_DO);
        $formAddTag = $this->createForm(TagType::class, $tag);

        $formAddTag->handleRequest($request);

        if ($formAddTag->isSubmitted() && $formAddTag->isValid()) {

            $em->persist($tag);
            try {
                $em->flush();
                $this->get('alert')->success('The tag "' . $tag->getTagName() . '" has been added.');
                return $this->redirectToRoute('job_admin_tags_what_user_do_show');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error($exception->getMessage(), 'Tag not added.');
                return $this->redirectToRoute('job_admin_tags_what_user_do_show');
            }

        }

        $tagsRep = $em->getRepository(Tag::class);
        $tagList = $tagsRep->loadAllTagsAsArray(Tag::TAG_WHAT_USER_DO);

        return $this->render('@view.job_admin/TagWhatUserDo/modifyTags.html.twig', [
            'formAddTag'            => $formAddTag->createView(),
            'tagList'               => $tagList,
        ]);
    }
}
