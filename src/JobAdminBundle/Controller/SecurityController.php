<?php

namespace JobAdminBundle\Controller;

use Doctrine\ORM\EntityManager;
use JobAdminBundle\Entity\Tag;
use JobAdminBundle\Entity\User;
use JobAdminBundle\Entity\UserInformation;
use JobAdminBundle\Form\FormType\LoginType;
use JobAdminBundle\Repository\TagRepository;
use JobBundle\Form\FormClass\ChangeUserInformation;
use JobBundle\Form\FormType\ChangeUserInformationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Vich\UploaderBundle\Handler\DownloadHandler;

class SecurityController extends Controller
{
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {
        $formLogin = $this->createForm(LoginType::class);

        $formLogin->handleRequest($request);

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        if ($error) $this->get('alert')->error($error->getMessage());

        return $this->render('@view.job_admin/Security/login.html.twig', [
            'error'             => $error,
            'lastUserName'      => $lastUsername,
            'formLogin'         => $formLogin->createView(),
        ]);
    }

    public function logout(Request $request)
    {}

    public function profile(Request $request)
    {

        $currentUser = $this->getUser();

        return $this->render('@view.job_admin/Security/my_profile.html.twig', [
        ]);

    }

    public function profileChange(Request $request)
    {

        /**
         * @var $handler                    DownloadHandler
         * @var $currentUser                User
         * @var $em                         EntityManager
         * @var $tagRep                     TagRepository
         */
        $em = $this->getDoctrine()->getManager();

        $currentUser = $this->getUser();
        if (!$currentUser->getUserInformation()) $userInformation = new UserInformation();
        else $userInformation = $currentUser->getUserInformation();

        $informationUser = new ChangeUserInformation();
        $informationUser->setUser($currentUser);
        $informationUser->setUserInformation($userInformation);


        $form = $this->createForm(ChangeUserInformationType::class, $informationUser, [
            'validation_groups' => ['update_user'],
        ]);
        $form->handleRequest($request);

        $handler = $this->get('vich_uploader.download_handler');

        if ($form->isSubmitted() && $form->isValid()) {

            $currentUser->setUserInformation($userInformation);
            $userInformation->setUser($currentUser);

            dump($currentUser);

            try {
                $em->flush();
                $this->get('alert')->success($currentUser->getUsername() . ' is Updated!');
                return $this->redirectToRoute('job_admin_security_my_profile');

            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error($exception->getMessage());
                return $this->redirectToRoute('job_admin_security_my_profile');
            }

        }

        return $this->render('@view.job_admin/Security/my_profile_change.html.twig', [
            'change_user_information_form'      => $form->createView(),
        ]);

    }

}
