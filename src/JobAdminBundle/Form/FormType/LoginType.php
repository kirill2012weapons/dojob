<?php

namespace JobAdminBundle\Form\FormType;


use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label'     => 'Your Email',
            ])
            ->add('password', PasswordType::class, [
                'label'     => 'Your Password',
            ])
        ;
    }

    public function getName()
    {
        return 'login_form';
    }


}