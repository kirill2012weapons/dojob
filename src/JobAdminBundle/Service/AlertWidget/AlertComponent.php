<?php

namespace JobAdminBundle\Service\AlertWidget;


use Symfony\Component\HttpFoundation\Session\Session;

abstract class AlertComponent
{

    /**
     * @var \Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface
     */
    private $flash;

    public function __construct(Session $session)
    {
        $this->flash = $session->getFlashBag();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface
     */
    public function getFlash()
    {
        return $this->flash;
    }

    abstract public function error($message, $title = null);
    abstract public function warning($message, $title = null);
    abstract public function success($message, $title = null);
    abstract public function info($message, $title = null);

    public function getMessage()
    {
        foreach ($this->getFlash()->get('alerts') as $message) {
            echo $message;
        }
    }

    public function jsOptions()
    {
        $option = <<<toastr_str
<script>
$( document ).ready(function() {
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "progressBar": true,
      "preventDuplicates": false,
      "positionClass": "toast-top-right",
      "onclick": null,
      "showDuration": "400",
      "hideDuration": "1000",
      "timeOut": "7000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
});
</script>
toastr_str;

        echo $option;

    }

}