<?php

namespace JobAdminBundle\Service\AlertWidget;


class Alert extends AlertComponent
{

    public function error($message, $title = null)
    {
        $script = <<<js_script
<script>
$( document ).ready(function() {
    toastr.error('{$message}', '{$title}');
});
</script>
js_script;

        $this->getFlash()->add('alerts', $script);

    }

    public function success($message, $title = null)
    {
        $script = <<<js_script
<script>
$( document ).ready(function() {
    toastr.success('{$message}', '{$title}');
});
</script>
js_script;

        $this->getFlash()->add('alerts', $script);
    }

    public function warning($message, $title = null)
    {
        $script = <<<js_script
<script>
$( document ).ready(function() {
    toastr.warning('{$message}', '{$title}');
});
</script>
js_script;

        $this->getFlash()->add('alerts', $script);
    }

    public function info($message, $title = null)
    {
        $script = <<<js_script
<script>
$( document ).ready(function() {
    toastr.info('{$message}', '{$title}');
});
</script>
js_script;

        $this->getFlash()->add('alerts', $script);
    }

}