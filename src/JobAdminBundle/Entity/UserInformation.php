<?php
namespace JobAdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Naming\UniqidNamer;

/**
 * @ORM\Table(name="job_users_information")
 * @ORM\Entity()
 */
class UserInformation
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     * @ORM\OneToOne(targetEntity="User", inversedBy="userInformation")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\Regex(
     *     pattern="/^[A-Za-zА-Яа-яЁё0-9\n\-\'\@\#\$\?\!]$/",
     *     message="Invalid Text"
     * )
     */
    private $tagWhatUserDo;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Assert\Regex(
     *     pattern="/^[A-Za-zА-Яа-яЁё0-9\-\'\!\?\#\@\\|\*\+\n]+$/",
     *     message="Invalid Text"
     * )
     */
    private $userDescription;

    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getTagWhatUserDo()
    {
        return $this->tagWhatUserDo;
    }

    /**
     * @param string $tagWhatUserDo
     */
    public function setTagWhatUserDo($tagWhatUserDo)
    {
        $this->tagWhatUserDo = $tagWhatUserDo;
    }

    /**
     * @return string
     */
    public function getUserDescription()
    {
        return $this->userDescription;
    }

    /**
     * @param string $userDescription
     */
    public function setUserDescription($userDescription)
    {
        $this->userDescription = $userDescription;
    }

}