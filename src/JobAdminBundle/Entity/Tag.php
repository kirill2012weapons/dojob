<?php
namespace JobAdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="job_tags")
 * @ORM\Entity(repositoryClass="JobAdminBundle\Repository\TagRepository")
 */
class Tag
{

    const TAG_WHAT_USER_DO = 'what_user_do';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(
     *     type="string"
     * )
     * @Assert\Regex(
     *     pattern="/^[A-Za-z0-9\_]+$/",
     *     message="Not Valid Field"
     * )
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(
     *     type="string"
     * )
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^[A-Za-z0-9\s\-\_\'\@\#]+$/",
     *     message="Value is not valid!"
     * )
     */
    private $tagName;

    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getTagName()
    {
        return $this->tagName;
    }

    /**
     * @param string $tagName
     */
    public function setTagName($tagName)
    {
        $this->tagName = $tagName;
    }

    public function __toString()
    {
        return $this->getTagName();
    }

}