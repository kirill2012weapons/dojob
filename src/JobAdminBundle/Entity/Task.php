<?php
namespace JobAdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JobAdminBundle\Entity\Interfaces\IProcessing;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="job_tasks")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Task implements IProcessing
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $dateTaskStart;

    /**
     * @var \DateTime
     * @ORM\Column(type="string")
     */
    private $dateTaskEnd;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $process;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $modifiedAt;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="tasks")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    private $project;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Checker", mappedBy="task")
     */
    private $checkers;

    public function __construct()
    {
        $this->checkers             = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getDateTaskStart()
    {
        return $this->dateTaskStart;
    }

    /**
     * @param \DateTime $dateTaskStart
     */
    public function setDateTaskStart($dateTaskStart)
    {
        $this->dateTaskStart = $dateTaskStart;
    }

    /**
     * @return \DateTime
     */
    public function getDateTaskEnd()
    {
        return $this->dateTaskEnd;
    }

    /**
     * @param \DateTime $dateTaskEnd
     */
    public function setDateTaskEnd($dateTaskEnd)
    {
        $this->dateTaskEnd = $dateTaskEnd;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return ArrayCollection
     */
    public function getCheckers()
    {
        return $this->checkers;
    }

    /**
     * @param ArrayCollection $checkers
     */
    public function setCheckers($checkers)
    {
        $this->checkers = $checkers;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

}