<?php
namespace JobAdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JobAdminBundle\Entity\Interfaces\IProcessing;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="job_projects")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Project implements IProcessing
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $projectName;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $projectInformation;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $dateProjectStart;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $dateProjectAnd;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $process;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $modifiedAt;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="User", mappedBy="projects")
     */
    private $users;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Task", mappedBy="project")
     */
    private $tasks;

    public function __construct()
    {
        $this->users            = new ArrayCollection();
        $this->tasks            = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * @param string $projectName
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }

    /**
     * @return string
     */
    public function getProjectInformation()
    {
        return $this->projectInformation;
    }

    /**
     * @param string $projectInformation
     */
    public function setProjectInformation($projectInformation)
    {
        $this->projectInformation = $projectInformation;
    }

    /**
     * @return \DateTime
     */
    public function getDateProjectStart()
    {
        return $this->dateProjectStart;
    }

    /**
     * @param \DateTime $dateProjectStart
     */
    public function setDateProjectStart($dateProjectStart)
    {
        $this->dateProjectStart = $dateProjectStart;
    }

    /**
     * @return \DateTime
     */
    public function getDateProjectAnd()
    {
        return $this->dateProjectAnd;
    }

    /**
     * @param \DateTime $dateProjectAnd
     */
    public function setDateProjectAnd($dateProjectAnd)
    {
        $this->dateProjectAnd = $dateProjectAnd;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param ArrayCollection $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @return ArrayCollection
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param ArrayCollection $tasks
     */
    public function setTasks($tasks)
    {
        $this->tasks = $tasks;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

}