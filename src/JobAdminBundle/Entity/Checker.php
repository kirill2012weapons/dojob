<?php
namespace JobAdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JobAdminBundle\Entity\Interfaces\IProcessing;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="job_checkers")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Checker implements IProcessing
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $checkInformation;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $process;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $modifiedAt;

    /**
     * @var Task
     * @ORM\ManyToOne(targetEntity="Task", inversedBy="checkers")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     */
    private $task;

    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCheckInformation()
    {
        return $this->checkInformation;
    }

    /**
     * @param string $checkInformation
     */
    public function setCheckInformation($checkInformation)
    {
        $this->checkInformation = $checkInformation;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param Task $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }


}