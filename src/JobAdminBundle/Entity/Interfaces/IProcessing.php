<?php

namespace JobAdminBundle\Entity\Interfaces;


interface IProcessing
{

    const PROCESS_IS_ACTIVE         = 'is_active';
    const PROCESS_IS_CLOSE          = 'is_close';
    const PROCESS_IS_IN_PROCESSING  = 'is_in_processing';

    public function getProcess();

    public function setProcess($process);

}