<?php
namespace JobAdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="job_users")
 * @ORM\Entity(repositoryClass="JobAdminBundle\Repository\UserRepository")
 * @UniqueEntity(
 *     fields={"username", "email"}
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface, AdvancedUserInterface, \Serializable
{
    const ROLE_USER                 = 'ROLE_USER';
    const ROLE_COMPANY_USER         = 'ROLE_COMPANY_USER';
    const ROLE_COMPANY_MANAGER      = 'ROLE_COMPANY_MANAGER';
    const ROLE_COMPANY_DIRECTOR     = 'ROLE_COMPANY_DIRECTOR';
    const ROLE_SUPER_ADMIN          = 'ROLE_SUPER_ADMIN';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     * @Assert\NotBlank(groups={"creating_user", "update_user"})
     * @Assert\NotBlank(groups={"creating_user", "update_user"})
     * @Assert\Regex(
     *     groups={"creating_user", "update_user"},
     *     pattern="/^([A-Za-z0-9\s\'\-\_]{3,12})$/",
     *     message="Contain Latine, Numbers, [ - ], [ ], [ ' ], [ _ ]. Length from 3 to 12."
     * )
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\NotBlank(groups={"creating_user"})
     * @Assert\NotNull(groups={"creating_user"})
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=254, unique=true)
     * @Assert\NotNull(groups={"creating_user"})
     * @Assert\NotBlank(groups={"creating_user"})
     * @Assert\Email(groups={"creating_user"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=254)
     * @Assert\NotBlank(groups={"creating_user", "update_user"})
     * @Assert\NotBlank(groups={"creating_user", "update_user"})
     * @Assert\Regex(
     *     groups={"creating_user", "update_user"},
     *     pattern="/^([A-Za-z\s\'\-]{3,15})$/",
     *     message="Contain Latine, Numbers, [-], [ ], [']. Length from 3 to 15."
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=254)
     * @Assert\NotBlank(groups={"creating_user", "update_user"})
     * @Assert\NotBlank(groups={"creating_user", "update_user"})
     * @Assert\Regex(
     *     groups={"creating_user", "update_user"},
     *     pattern="/^([A-Za-z\s\'\-]{3,15})$/",
     *     message="Contain Latine, Numbers, [-], [ ], [']. Length from 3 to 15."
     * )
     */
    private $surname;


    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $modifiedAt;

    /**
     * @var Company
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="users")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private $company;

    /**
     * One Customer has One Cart.
     * @ORM\OneToOne(targetEntity="UserInformation", mappedBy="user", cascade={"persist"})
     */
    private $userInformation;

    /**
     * @ORM\ManyToMany(targetEntity="Tag", cascade={"remove"})
     * @ORM\JoinTable(name="users_tags",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $tags;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Project", inversedBy="users")
     * @ORM\JoinTable(name="users_projects")
     */
    private $projects;

    public function __construct()
    {
        $this->isActive         = true;
        $this->setRoles( self::ROLE_USER );
        $this->tags             = new ArrayCollection();
        $this->projects         = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * @param ArrayCollection $projects
     */
    public function setProjects($projects)
    {
        $this->projects = $projects;
    }

    /**
     * @return ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param Tag $tags
     */
    public function setTags($tags)
    {
        $this->tags->add($tags);
    }

    /**
     * @return UserInformation
     */
    public function getUserInformation()
    {
        return $this->userInformation;
    }

    /**
     * @param UserInformation $userInformation
     */
    public function setUserInformation($userInformation)
    {
        $this->userInformation = $userInformation;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getSalt()
    {
        return null;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setRoles($role)
    {
        $this->roles[] = $role;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            $this->name,
            $this->surname,
        ]);
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            $this->name,
            $this->surname,
            ) = unserialize($serialized, ['allowed_classes' => false]);
    }

}