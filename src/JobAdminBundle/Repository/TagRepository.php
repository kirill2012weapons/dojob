<?php

namespace JobAdminBundle\Repository;


use Doctrine\ORM\EntityRepository;

class TagRepository extends EntityRepository
{
    public function loadAllTagsAsArray($tagSlug)
    {
        return $this->createQueryBuilder('t')
            ->where('t.slug = :tagSlug')
            ->setParameter('tagSlug', $tagSlug)
            ->getQuery()
            ->getArrayResult()
        ;
    }

    public function loadAllTagsQuery($tagSlug)
    {
        return $this->createQueryBuilder('t')
            ->where('t.slug = :tagSlug')
            ->setParameter('tagSlug', $tagSlug)
        ;
    }
}