<?php

namespace JobBundle\Form\FormType;


use Doctrine\ORM\EntityRepository;
use JobAdminBundle\Entity\Tag;
use JobAdminBundle\Repository\TagRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use JobAdminBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            $form = $event->getForm();
            /**
             * @var $builder FormBuilder
             */
            $builder = $form->getConfig();

            if (in_array('creating_user', $builder->getOptions()['validation_groups']))
                $form
                    ->add('password', RepeatedType::class, [
                        'type'      => PasswordType::class,
                        'invalid_message' => 'The password fields must match.',
                        'first_options'  => ['label' => 'Password'],
                        'second_options' => ['label' => 'Repeat Password'],
                    ])
                    ->add('email', EmailType::class, [
                        'label'     => 'Your Email',
                    ])
                ;

        });

        $builder
            ->add('username', TextType::class, [
                'label'     => 'Your Username',
            ])
            ->add('name', TextType::class, [
                'label'     => 'Your Name',
            ])
            ->add('surname', TextType::class, [
                'label'     => 'Your Surname',
            ])
            ->add('tags', EntityType::class, [
                'class'     => Tag::class,
                "multiple" => "true",
                'query_builder' => function (EntityRepository $er) {
                    /**
                     * @var $er             TagRepository
                     */
                    return $er->loadAllTagsQuery(Tag::TAG_WHAT_USER_DO);
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);

    }

    public function getName()
    {
        return 'user_form';
    }


}