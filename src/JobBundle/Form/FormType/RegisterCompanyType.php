<?php

namespace JobBundle\Form\FormType;


use JobBundle\Form\FormClass\RegisterCompany;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterCompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('company', CompanyFormType::class, [
                'by_reference' => true,
            ])
            ->add('user', UserFormType::class, [
                'by_reference' => true,
                'validation_groups' => ['creating_user'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RegisterCompany::class,
        ]);

    }

    public function getName()
    {
        return 'register_company';
    }

}