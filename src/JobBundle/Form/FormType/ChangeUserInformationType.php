<?php

namespace JobBundle\Form\FormType;


use JobBundle\Form\FormClass\ChangeUserInformation;
use JobBundle\Form\FormClass\RegisterCompany;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangeUserInformationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('userInformation', UserInformationType::class, [
                'by_reference' => true,
            ])
            ->add('user', UserFormType::class, [
                'by_reference' => true,
                'validation_groups' => ['update_user'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ChangeUserInformation::class,
        ]);

    }

    public function getName()
    {
        return 'change_user_information';
    }

}