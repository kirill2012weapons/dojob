<?php

namespace JobBundle\Form\FormType;


use JobAdminBundle\Entity\UserInformation;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserInformationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tagWhatUserDo', TextType::class, [
                'label'     => 'What I Do?',
            ])
            ->add('userDescription', TextType::class, [
                'label'     => 'Your Description',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserInformation::class,
        ]);

    }

    public function getName()
    {
        return 'user_information';
    }


}