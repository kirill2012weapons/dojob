<?php

namespace JobBundle\Form\FormClass;


use JobAdminBundle\Entity\Company;
use JobAdminBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

class RegisterCompany
{

    /**
     * @var User
     * @Assert\Valid()
     */
    private $user;

    /**
     * @var Company
     * @Assert\Valid()
     */
    private $company;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

}