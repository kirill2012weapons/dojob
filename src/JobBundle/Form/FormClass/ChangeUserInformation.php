<?php

namespace JobBundle\Form\FormClass;


use JobAdminBundle\Entity\UserInformation;
use JobAdminBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

class ChangeUserInformation
{

    /**
     * @var User
     * @Assert\Valid()
     */
    private $user;

    /**
     * @var UserInformation
     * @Assert\Valid()
     */
    private $userInformation;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return UserInformation
     */
    public function getUserInformation()
    {
        return $this->userInformation;
    }

    /**
     * @param UserInformation $userInformation
     */
    public function setUserInformation($userInformation)
    {
        $this->userInformation = $userInformation;
    }

}