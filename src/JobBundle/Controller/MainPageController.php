<?php

namespace JobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainPageController extends Controller
{
    public function indexAction()
    {
        return $this->render('@view.job/MainPage/index.html.twig');
    }
}
