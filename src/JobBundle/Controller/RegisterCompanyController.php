<?php

namespace JobBundle\Controller;


use Doctrine\ORM\EntityManager;
use JobAdminBundle\Entity\Company;
use JobAdminBundle\Entity\User;
use JobBundle\Form\FormClass\RegisterCompany;
use JobBundle\Form\FormType\RegisterCompanyType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Tests\Encoder\PasswordEncoder;

class RegisterCompanyController extends Controller
{

    public function registerNewCompany(Request $request)
    {

        /**
         * @var $em                 EntityManager
         * @var $passwordEncoder    PasswordEncoder
         */

        $user               = new User();
        $company            = new Company();
        $registerCompany    = new RegisterCompany();
        $registerCompany->setUser($user);
        $registerCompany->setCompany($company);

        $form = $this->createForm(RegisterCompanyType::class, $registerCompany, [
            'validation_groups' => ['creating_user'],
        ]);

        dump($form->createView());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $passwordEncoder = $this->get('security.password_encoder');

            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $user->setRoles( User::ROLE_COMPANY_DIRECTOR );
            $user->setCompany($company);
            $company->setUsers($user);

            $em->persist($user);
            $em->persist($company);

            try {
                $em->flush();
                $this->get('alert')->success('Success Registration. Welcome!');
                return $this->redirectToRoute('job_admin_login');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error('Cannon create User or Company.');
            }

        }
        if ($form->isSubmitted() && !$form->isValid()) $this->get('alert')->error('Form Not Valid.');


        return $this->render('@view.job/RegisterCompany/index.html.twig', [
            'newCompanyForm'            => $form->createView(),
        ]);
    }

}