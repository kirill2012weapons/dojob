<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190813131715 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users_tags DROP FOREIGN KEY FK_A035DA28A76ED395');
        $this->addSql('ALTER TABLE users_tags DROP FOREIGN KEY FK_A035DA28BAD26311');
        $this->addSql('ALTER TABLE users_tags ADD CONSTRAINT FK_A035DA28A76ED395 FOREIGN KEY (user_id) REFERENCES job_users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_tags ADD CONSTRAINT FK_A035DA28BAD26311 FOREIGN KEY (tag_id) REFERENCES job_tags (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users_tags DROP FOREIGN KEY FK_A035DA28A76ED395');
        $this->addSql('ALTER TABLE users_tags DROP FOREIGN KEY FK_A035DA28BAD26311');
        $this->addSql('ALTER TABLE users_tags ADD CONSTRAINT FK_A035DA28A76ED395 FOREIGN KEY (user_id) REFERENCES job_users (id)');
        $this->addSql('ALTER TABLE users_tags ADD CONSTRAINT FK_A035DA28BAD26311 FOREIGN KEY (tag_id) REFERENCES job_tags (id)');
    }
}
