<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190813130847 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE job_companies (id INT AUTO_INCREMENT NOT NULL, company_name VARCHAR(25) NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_45F4452B1D4E64E8 (company_name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file_custom (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(255) NOT NULL, file_name VARCHAR(255) NOT NULL, file_size INT NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job_tags (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(255) NOT NULL, tag_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job_users (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, username VARCHAR(25) NOT NULL, password VARCHAR(64) NOT NULL, email VARCHAR(254) NOT NULL, name VARCHAR(254) NOT NULL, surname VARCHAR(254) NOT NULL, is_active TINYINT(1) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_at DATETIME NOT NULL, modified_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_15DF1899F85E0677 (username), UNIQUE INDEX UNIQ_15DF1899E7927C74 (email), INDEX IDX_15DF1899979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_tags (user_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_A035DA28A76ED395 (user_id), INDEX IDX_A035DA28BAD26311 (tag_id), PRIMARY KEY(user_id, tag_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job_users_information (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, tag_what_user_do VARCHAR(255) NOT NULL, user_description LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_DACAE484A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE job_users ADD CONSTRAINT FK_15DF1899979B1AD6 FOREIGN KEY (company_id) REFERENCES job_companies (id)');
        $this->addSql('ALTER TABLE users_tags ADD CONSTRAINT FK_A035DA28A76ED395 FOREIGN KEY (user_id) REFERENCES job_users (id)');
        $this->addSql('ALTER TABLE users_tags ADD CONSTRAINT FK_A035DA28BAD26311 FOREIGN KEY (tag_id) REFERENCES job_tags (id)');
        $this->addSql('ALTER TABLE job_users_information ADD CONSTRAINT FK_DACAE484A76ED395 FOREIGN KEY (user_id) REFERENCES job_users (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE job_users DROP FOREIGN KEY FK_15DF1899979B1AD6');
        $this->addSql('ALTER TABLE users_tags DROP FOREIGN KEY FK_A035DA28BAD26311');
        $this->addSql('ALTER TABLE users_tags DROP FOREIGN KEY FK_A035DA28A76ED395');
        $this->addSql('ALTER TABLE job_users_information DROP FOREIGN KEY FK_DACAE484A76ED395');
        $this->addSql('DROP TABLE job_companies');
        $this->addSql('DROP TABLE file_custom');
        $this->addSql('DROP TABLE job_tags');
        $this->addSql('DROP TABLE job_users');
        $this->addSql('DROP TABLE users_tags');
        $this->addSql('DROP TABLE job_users_information');
    }
}
